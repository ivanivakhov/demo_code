<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StageAssessments extends Model
{
    protected $table = '***';
    protected $fillable = [
        'stage', /*...*/
    ];

    public function storeAssessments (
        $reportId,
        $competenciesNotes
    )
    {

        $collection = [];
        foreach($competenciesNotes as $index => $competency) {
            $collection[$index]['reportId'] = $reportId;
            /*...*/
        }

        $this->insert($collection);
        return $collection;
    }

    public function getAssessments(int $stageReportId)
    {
        return self::where(['reportId' => $stageReportId])
            ->leftjoin()
            ->leftjoin()
            ->get([]);
    }
}
