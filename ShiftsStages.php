<?php

namespace App;

use App\Http\Controllers\PolicemanController;
use Exception;
use Illuminate\Database\Eloquent\Model;

class ShiftsStages extends Model
{
    protected $table = "*";

    public $primaryKey = 'id';
    public $timestamps = true;


    protected $fillable = [
        'stage_id', /*...*/
    ];
    private $user_id;


    private function getLatestShift ($traineeId)
    {
        $response = null;
        try {
            $response = self::where();

        } catch (Exception $e) {
            $response = $e->getMessage();
        }
//        dd(self::where('trainee_id', $traineeId)->orderBy('trainee_start', 'desc')->first());
        return $response;
    }

    private function findTraineeIfMentor ($traineeId)
    {
        $user = new User();

        $userRole = $user->getUserRoleById($traineeId);
        if ($userRole->slug === 'mentor') {
            $traineeId = $user->getPartner()['id'];
        } elseif ($userRole->name === 'trainee') {
            return $traineeId;
        }

        return $traineeId;
    }
    public function latestShiftInfo ($traineeId)
    {
        $traineeId = $this->findTraineeIfMentor ($traineeId);
        $response = $this->getLatestShift($traineeId);

        if ($response) {
            $success['shift'] = $response->getOriginal('shift');
            /*...*/
        } else {
            $success['shift'] = null;
            /*...*/
        }

        return $success;
    }

    private function startNewStage ($latestShift, $shiftsInStage = 3,  $traineeId, $userRole)
    {
//        $latestShift = $this->getLatestShift($traineeId);
        if (is_null($latestShift['stage'])) {
            //user has just started new program
            //start first stage and shift
            $this->stage = 1;
            $this->shift = 1;
            return $this;
        }

        $shiftsInStage1 = 3;
        $shiftsInStage2 = 12;
        $shiftsInStage3 = 15;

        switch ($latestShift['stage']) {
            case 1:
                $shiftsInStage = $shiftsInStage1;
                break;
            case 2:
                $shiftsInStage = $shiftsInStage2;
                break;
            case 3:
                $shiftsInStage = $shiftsInStage3;
                break;
            default:
                $shiftsInStage = $shiftsInStage1;
        }

        if ($latestShift['shift'] < $shiftsInStage) {
            $this->stage = $latestShift['stage'];
            $this->shift = $latestShift['shift'] + 1;
        }
        if ($latestShift['shift'] === $shiftsInStage) {


            if ($userRole !== 'trainee') {
                //TODO:: check if trainee by id, because userRole slug is set on frontend so can be injected
                return ['error' => [
                        'message' => 'Only trainee can start new shift'
                ]];
            }
            $userRoleSlug = (new User())->getUserRoleById($traineeId)->slug;
            if ($userRoleSlug !== 'trainee') {
                return ['error' => [
                    'message' => 'Only trainee can start new shift'
                ]];
            }
            $latestStageReport = (new StageReport())->getStageReportLatest( $traineeId);


            if ($latestStageReport->go_next === 0) {
                //if not approved to go next


                return ['error' => [
                    'message' => 'Not approved to go next'
                ]];

            } else {
                $this->stage = $latestShift['stage'] + 1;
                $this->shift = 1;
            }

        }
        return $this;
    }

    public function startNewShift ($traineeId, $userRole)
    {
        $latestShift = $this->getLatestShift($traineeId);
        if ($latestShift !== null && $latestShift->mentor_finish === null){
            return $latestShift;
        }

        if ($userRole !== 'trainee') {
            return;
        }
        $startStage = $this->startNewStage($latestShift, 3, $traineeId, $userRole);
        if (!$startStage) {
            return false;
        }
        if (isset($startStage['error'])) {
            return $startStage;
        }
//        dd($startStage);
        try {
            $this->start = date('Y-m-d H:i:s');
            /*...*/
            $this->save();
        } catch(Exception $e) {
            return ['error' => [
                'code' => $e->getCode()
            ]];
        }

        return $this;
    }

    public function confirmNewShift ($mentorId, $userRole)
    {
//        dd($mentorId, $userRole);
        if ($userRole !== 'mentor') {
            return;
        }
        $traineeId = $this->findTraineeIfMentor($mentorId);

        $latestShift = $this->getLatestShift($traineeId);

        try {
            $latestShift->start = date('Y-m-d H:i:s');
            /*...*/
            $latestShift->update();
        } catch (\Exception $e) {
            return ['error' => [
                'code' => $e->getCode(),
                'message' => 'Помилка сервера',
//                'message' => $e->getMessage(),
            ]];
        }

        return $latestShift;
    }


    public function finishShift ($traineeId, $currentShift, $stage, $userRole)
    {
        try {
            $shiftToFinish = self::where(['trainee_id' => $traineeId, 'shift' => $currentShift, 'stage' => $stage])->first();

            if ($shiftToFinish) {
                $shiftToFinish->timestamps = false;
                if ($userRole === 'trainee') {
                    $shiftToFinish->finish = date('Y-m-d H:i:s');
                    $shiftToFinish->update();
                } elseif($userRole === 'mentor') {
                    $shiftToFinish->finish = date('Y-m-d H:i:s');
                    $shiftToFinish->update();
                }
            } else {
                $shiftToFinish = ['error' => [
                    'message' => 'Неможливо знайти зміну або етап'
                ]];
            }
        } catch (Exception $exception){
            $shiftToFinish = ['error' => [
                'message' => 'Помилка сервера',
                'code' => $exception->getCode(),
            ]];
        }

        return $shiftToFinish;
    }
}
