<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competencies extends Model
{
    protected $table = '*';
    public static function listCompetencies ()
    {
        return self::get();
    }
}
