<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalReports extends Model
{
    protected $table = '***';
    protected $fillable = [
        'trainee_id',
        'mentor_id',/*...*/
    ];

    public function saveFinalReport (
        $trainee_id,
        $mentor_id,
        $internship_started,
        $internship_ended,
        $characteristics,
        $general_grade,
        $competenciesAsses
    )
    {
        $report = $this->FinalReportIfExists ($trainee_id, $mentor_id);

        if ($report) {
            $report->general_grade = $general_grade;
            /*....*/

            try {
                $report->save();

                $mentorsCompNotes = (new FinalCompetenciesAssess())->storeAssessments($report->id, $competenciesAsses);
            } catch (\Exception $e) {
                return ['error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]];
            }
            return [$report, $mentorsCompNotes];

        } else {
            try {
                $this->trainee_id = $trainee_id;
                $this->mentor_id = $mentor_id;
                /*...*/
                $this->save();
            } catch (\Exception $e){
                return ['error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]];
            }
            $mentorsCompNotes = (new FinalCompetenciesAssess())->storeAssessments($this->id, $competenciesAsses);
            return [$this, $mentorsCompNotes];
        }
    }

    private function FinalReportIfExists (int $traineeId, int $mentorId)
    {
        $report = self::where([/*...*/])->orderBy(/*...*/)->first();

        if ($report) {
            return $report;
        } else {
            return false;
        }
    }
}
