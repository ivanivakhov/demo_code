<?php

namespace App\Http\Controllers;

use App\ShiftReport;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShiftReportController extends Controller
{
    public function createShiftReport (Request $request, ShiftReport $shiftReport)
    {
        return response()->json($shiftReport->createShiftReport(
            $request->trainee_id,
            $request->mentor_id,
            $request->shift,
            $request->stage,
            $request->started_at,
            $request->is_day_shift,
            $request->mentors_recommendations,
            $request->next_shift_date,
            $request->competencies_notes
        ));
    }
    public function saveShiftReport (Request $request, ShiftReport $shiftReport)
    {
        $user = User::getUser();


        if($user->role === 3){
            //trainee
           return  $shiftReport->saveTraineesReport(
                $request->trainee_id,
                $request->mentor_id,
                $request->shift,
                $request->stage,
                $request->started_at,
                $request->is_day_shift,
                $request->competencies_notes,
                $request->situations_statistics
            );
        } else if ($user->role === 2){
            //mentor
            $shiftReport->saveMentorsReport(
                $request->trainee_id,
                $request->mentor_id,
                $request->shift,
                $request->stage,
                $request->started_at,
                $request->is_day_shift,
                $request->mentors_recommendations,
                $request->next_shift_date,
                $request->competencies_notes
            );
        }

        dd($user);

        return response()->json($shiftReport->saveShiftReport(
            $request->trainee_id,
            $request->mentor_id,
            $request->shift,
            $request->stage,
            $request->started_at,
            $request->is_day_shift,
            $request->mentors_recommendations,
            $request->next_shift_date,
            $request->competencies_notes
        ));
    }
}
