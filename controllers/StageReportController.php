<?php

namespace App\Http\Controllers;

use App\StageReport;
use Illuminate\Http\Request;

class StageReportController extends Controller
{
    public function createStageReport (Request $request, StageReport $report)
    {
        $report = $report->createStageReport(
            $request->started_at,
            $request->finished_at,
            $request->stage,
            $request->mentorComment,
            $request->competenciesNotes
        );

        return $report;
    }

    public function showStageReport (Request $request, StageReport $report)
    {

        return $report->showStageReport($request->id);
    }
    public function saveTroopRecommendations (Request $request, StageReport $report)
    {
        return response()->json($report->saveTroopRecommendations($request->stage_report_id, $request->troop_recommendations, $request->add_shifts, $request->go_next));
    }
    public function getStageReports (Request $request, StageReport $report)
    {
        return response()->json($report->getStageReports($request->trainee_id));
    }

}
