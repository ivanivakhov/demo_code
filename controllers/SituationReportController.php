<?php


namespace App\Http\Controllers;


use App\SituationReport;
use App\User;
use Illuminate\Http\Request;

class SituationReportController extends Controller
{
    public function listSituationReport(Request $request, SituationReport $report)
    {
        $userId = $request->user_id;

        return $report->listSituationReport($userId);
    }

    public function freshSituationReports (Request $request, SituationReport $report)
    {
        $userId = $request->user_id;
        $shift = $request->shift;
        $stage = $request->stage;
        $shiftFinished = $request->shift_finished;
        $fresh_reports = $report->freshSituationReports($userId, $shift, $stage, $shiftFinished);
        return response()->json(['success' => $fresh_reports]);

    }

    public function listPartnersSituationReport (Request $request)
    {
        $partnerId = $request->partner_id;
        $reports = SituationReport::where('user_id', $partnerId)->get();
        return response()->json(['success' => $reports]);
    }

    public function storeSituationReport (Request $request, SituationReport $report)
    {
        $report = $report->storeSituationReport($request->email, $request->situation, $request->fabula, $request->algorithm, $request->competencies, $request->notes, $request->stage, $request->shift);

        return $report;
    }

    public function updateSituationReport (Request $request, SituationReport $report)
    {

        $report = $report->updateSituationReport($request->id, $request->situation, $request->fabula, $request->algorithm, $request->competencies, $request->notes);
        return $report;
    }

    public function addMentorsRecommendations (Request $request, SituationReport $report)
    {
        $reportId = $request->id;
        $report = $report::where('id', $reportId)->first();

        $report->recommendations = $request->recommendations;
//        $report->rating = $request->rating;

        try {
            $report->update();

        } catch (\Exception $exception) {
            $report = $exception->getMessage();
        }
        return response()->json(['success' => $report]);
    }

    public function showSituationReport (Request $request, SituationReport $report)
    {
        $id = $request->id;

        return $report->showSituationReport($id);
    }
}
