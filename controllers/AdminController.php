<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function adminInstance ()
    {
        return (new Admin());
    }
    public function getTraineesList ()
    {
        return $this->adminInstance()->getTraineesList();
    }
    public function getTraineesListByTroopBatt (Request $request)
    {
        return $this->adminInstance()->getTraineesListByTroopBatt($request->troop, $request->battalion);
    }

    public function getMentorsList ()
    {
        return $this->adminInstance()->getMentorsList();
    }
    public function getMentorsListByTroopBatt (Request $request)
    {
        return $this->adminInstance()->getMentorsListByTroopBatt($request->troop, $request->battalion);
    }

    public function getUsersByTroopBatt (Request $request)
    {
        return $this->adminInstance()->getUsersByTroopBatt($request->troop, $request->battalion);
    }
    public function getUsersNotInTeam ()
    {
        return $this->adminInstance()->getUsersNotInTeam();
    }
    public function listRoles ()
    {
        return $this->adminInstance()->listRoles();
    }
    public function changeRole (Request $request)
    {
        return $this->adminInstance()->changeRole($request->userId, $request->selectedRole);
    }
    public function changeTroop (Request $request)
    {
        return $this->adminInstance()->changeTroop($request->userId, $request->selectedTroop);
    }
    public function changeBattalion (Request $request)
    {
        return $this->adminInstance()->changeBattalion($request->userId, $request->selectedBattalion);
    }

    public function setTeam (Request $request)
    {
        $team = new Admin();
//        dd($request);
        $team = $team->setTeam($request->teams);

        return response()->json(['success' => $team]);
    }
}
