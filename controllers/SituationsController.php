<?php


namespace App\Http\Controllers;

use App\Situations;
use Illuminate\Http\Request;

class SituationsController
{
    public static function listSituations ()
    {
        return Situations::listSituations();
    }
}
