<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{

    public function getTeamList()
    {
        $teamModel = new Team();
        return $teamModel->getTeamListUserFriendly();
    }

}
