<?php


namespace App\Http\Controllers;


use App\FinalReports;
use App\User;
use Illuminate\Http\Request;

class FinalReportController extends Controller
{

    public function saveFinalReport (Request $request, FinalReports $finalReports)
    {

        $user = User::getUser();

        if ($user->role === 2){
            //if mentor

            $trainee_id = $request->trainee_id;
            $mentor_id = $request->mentor_id;
            $internship_started = $request->internship_started;
            $internship_ended = $request->internship_ended;
            $characteristics = $request->characteristics;
            $general_grade = $request->general_grade;
            $competenciesAsses = $request->competencies_grades;
//            dd($request);
//        $characteristics,
//        $general_grade,
//        $competenciesAsses,

            return $finalReports->saveFinalReport(
                $trainee_id,
                $mentor_id,
                $internship_started,
                $internship_ended,
                $characteristics,
                $general_grade,
                $competenciesAsses
            );
        }
    }
}
