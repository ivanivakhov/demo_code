<?php

namespace App\Http\Controllers;

use App\Competencies;
use Illuminate\Http\Request;

class CompetenciesController extends Controller
{
    public static function listCompetencies ()
    {
        return Competencies::listCompetencies();
    }
}
