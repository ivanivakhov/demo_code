<?php

namespace App\Http\Controllers;

use App\ShiftsStages;
use Illuminate\Http\Request;

class ShiftsStagesController extends Controller
{

    public function latestShiftInfo (Request $request, ShiftsStages $shiftsStages)
    {
        $stages = $shiftsStages->latestShiftInfo($request->trainee_id);
        return response()->json(['success' => $stages]);
    }

    public function startNewShift (Request $request, ShiftsStages $shiftsStages)
    {
        $start = $shiftsStages->startNewShift($request->trainee_id, $request->user_role);
        if (isset($start->error)) {
            return response()->json($start);
        }
        return response()->json(['success' => $start]);
    }
    public function confirmNewShift (Request $request, ShiftsStages $shiftsStages)
    {
        $start = $shiftsStages->confirmNewShift($request->trainee_id, $request->user_role);
        if (isset($start['error'])) {
//            dd($start);
            return $start;
        }
        return response()->json(['success' => $start]);
    }

    public function finishShift (Request $request, ShiftsStages $shiftsStages)
    {
        $finish = $shiftsStages->finishShift($request->trainee_id, $request->current_shift, $request->current_stage, $request->user_role);
        if (isset($finish['error'])) {
            return $finish;
        }
        return response()->json(['success' => $finish]);
    }
}
