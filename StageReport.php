<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StageReport extends Model
{
    protected $table = '*';
    protected $fillable = [
        'trainee_id', 'mentor_id', /*...*/
    ];
    public function createStageReport (
        $started_at,
        $finished_at,
        $stage,
        $mentorComment,
        $competenciesNotes
    )
    {
        $user = Auth::user(); //mentor id

        $mentorId = $user->id;
        $mentorRole = $user->getUserRoleById($mentorId)->slug;
        $traineeId = (new Team())->getPartner($user->role, $mentorRole, $mentorId);

        try {
            $this->trainee_id = $traineeId;
            $this->mentor_id = $mentorId;
            /*...*/
            $this->save();
        } catch (\Exception $exception) {

            return response()->json(['error' => 'Server error']);
        }

        $competenciesAssess = (new StageAssessments())->storeAssessments($this->id, $competenciesNotes);

        return [$this, $competenciesAssess];
    }

    public function getStageReports ($traineeId)
    {
        $reports = self::where([])->get()->toArray();

        $reports = array_map(function($v){
            $assessments = (new StageAssessments())->getAssessments($v['id'])->toArray();
            $v['a'] = $assessments;
            return $v;
        }, $reports);

        return response()->json(['success' => $reports]);
    }

    public function getStageReportLatest ($traineeId)
    {
        return self::where([])->orderBy()->first();
    }

    public static function listStageReports ()
    {
        return self::get();
    }

    public function showStageReport (int $id)
    {
        $report = self::find($id);
        $report['c'] = (new StageAssessments())->getAssessments($report->id);
//        dd($report);

        return response()->json(['success' => $report]);
    }

    public function getStageReportById (int $id)
    {
        return self::find($id);
    }
    public function saveTroopRecommendations (int $stageReportId, ?string $troopRecommendations, int $addShifts, bool $goNext)
    {
        try {
            $reportToUpdate = $this->getStageReportById($stageReportId);
            $reportToUpdate->tr = $troopRecommendations;
            /*...*/
            $reportToUpdate->update();
        } catch (\Exception $e) {
            return ['error' => [
                    'code' => $e->getCode()
                ]
            ];
        }

        return ['success' => $reportToUpdate];
    }

}
