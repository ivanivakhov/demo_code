<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalCompetenciesAssess extends Model
{
    protected $table = '*';
    protected $fillable = [
        'grade', /*...*/
    ];
    public function storeAssessments ($reportId, $competenciesGrades)
    {
        try{
            $collection = [];
            $finalReport = self::where([])->first();
            if($finalReport) {
                foreach($competenciesGrades as $index => $competency) {
                    $finalReport = self::where([])->first();
                    $finalReport->f = $reportId;

                    $finalReport->save();
                    $collection[] = $finalReport;
                }
            } else {
                foreach($competenciesGrades as $index => $competency) {
                    $collection[$index]['f'] = $reportId;
                }
                $this->insert($collection);
            }
        } catch (\Exception $e) {
            return ['error' => [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
            ]];
        }
        return ['success' => $collection];
    }
}
