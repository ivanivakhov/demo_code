<?php

namespace App;
use App\User;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Admin extends Model
{
    public function getTraineesList ()
    {
        return User::where(['***'])->get();
    }
    public function getTraineesListByTroopBatt ($troop, $battalion)
    {
        return DB::table('***')
            ->select('***.*')
            ->leftJoin('***', '***', '=', '***')
            ->where(['***', '***' => $battalion, '***' => $troop])
            ->whereNull('***')
            ->get();
    }

    public function getMentorsList ()
    {
        return User::where(['***'])->get();
    }
    public function getMentorsListByTroopBatt ($troop, $battalion)
    {
        return DB::table('***')
            ->select('***')
            ->leftJoin('***', '***', '=', '***')
            ->where(['***', '***' => $battalion, '***' => $troop])
            ->whereNull('***')
            ->get();
    }

    private function getUsersByRole ($role)
    {
        return User::where(['***' => $role])->get();
    }

    public function getUsersByTroopBatt ($troop, $battalion)
    {
        return User::where(['***' => $troop, '***' => $battalion])->get();
    }

    public function listRoles ()
    {
        return Role::get();
    }
    public function changeRole ($userId, $selectedRole)
    {

        $user = User::find($userId);

        $user->role = $selectedRole;
        $user->save();
        dd($user->toArray());
    }
    public function changeTroop ($userId, $selectedTroop)
    {

        $user = User::find($userId);

        $user->troop = $selectedTroop;
        $user->save();
        dd($user->toArray());
    }
    public function changeBattalion ($userId, $selectedBattalion)
    {

        $user = User::find($userId);

        $user->battalion = $selectedBattalion;
        $user->save();
        dd($user->toArray());
    }

    public function getUsersNotInTeam ()
    {
        return DB::table('***')/*...*/->get();
    }
    public function setTeam ($teamsToSet)
    {
        $teamTable = new Team;

        $teamsData = $teamTable::get()->toArray();

        $collection = [];
        $usersToSetInTeam = [];
        foreach ($teamsToSet as $index => $team) {

            //prevent creating team if user already in team
            $userInTeamAlready = false;
            foreach ($teamsData as $teamData) {
                //two last checks are not required, but if they return true, something has gone wrong
                if ($teamData['teamlead'] === $team['mentor']['id']
                    || $teamData['trainee'] === $team['trainee']['id']
                    || $teamData['trainee'] === $team['mentor']['id']
                    || $teamData['teamlead'] === $team['trainee']['id']
                ) {
                    $userInTeamAlready = true;
                }
            }
            if ($userInTeamAlready) continue;

            //prevent creating team if user appeared many times in $teamsToSet array
            if (array_search($team['mentor']['id'], $usersToSetInTeam)
                || array_search($team['trainee']['id'], $usersToSetInTeam)
            ) {
                continue;
            } else {
                $usersToSetInTeam[] = $team['mentor']['id'];
                $usersToSetInTeam[] = $team['trainee']['id'];
            }

            //adding to collection team data
            $collection[$index]['mentor'] = $team['mentor']['id'];
            /*...*/
        }

        $teamTable->insert($collection);

        return $collection;
    }

    public function setTroop ()
    {

    }

    public function setBattalion ()
    {

    }

    public function setRole ()
    {

    }
}
