// import Vuex from 'vuex';
//
// Vue.use(Vuex);
//
// const debug = process.env.NODE_ENV !== 'production';
import idbs from '@/js/indexedDBService'
export default {
    state: {
        todos: [],
        completed: [],
        dataFields: ['todos', 'completed']
    },
    mutations: {
        setState (state, { field, data }) {
            Vue.set(state, field, data)
        },
        addTodo (state, newTodo) {
            // console.log(newTodo);
            // console.log(state);
            // state.todos.push(newTodo)
        },
        deleteTodo (state, { todoIndex, isCompleted }) {
            if (isCompleted) {
                state.completed.splice(todoIndex, 1)
            } else {
                state.todos.splice(todoIndex, 1)
            }
        },
        completeTodo (state, todoIndex) {
            // console.log(state.completed);

            state.completed.push(state.todos.splice(todoIndex, 1)[0])
        }
    },
    actions: {
        addTodo ({ commit, dispatch }, newTodo) {
            commit('addTodo', newTodo)
            dispatch('saveTodos')
        },
        deleteTodo ({ commit, dispatch }, todoInfo) {
            commit('deleteTodo', todoInfo)
            dispatch('saveTodos')
        },
        completeTodo ({ commit, dispatch }, todoIndex) {
            commit('completeTodo', todoIndex)
            dispatch('saveTodos')
        },
        checkStorage ({ state, commit }) {
            state.dataFields.forEach(async field => {
                try {
                    let data = await idbs.checkStorage(field)
                    // console.log('checkStorage');
                    // IndexedDB did not find the data, try localStorage
                    if (data === undefined) {
                        data = 'hui';
                    }
                    // if (data === undefined) data = ls.checkStorage(field)
                    // LocalStorage did not find the data, reset it
                    if (data === null) {
                        data = [];
                    }
                    // console.log(data);

                    commit('setState', { field, data })
                } catch (e) {
                    // The value in storage was invalid or corrupt so just set it to blank
                    commit('setState', { field, data: [] })
                }
            })
        },
        async saveTodos ({ state }) {
            try {
                await Promise.all(state.dataFields.map(field => {
                        console.log(field);

                        return idbs.saveToStorage(field, state[field])}
                    )
                )
            } catch (e) {
                state.dataFields.forEach(field => {
                    console.log('asdfadsfadf');
                    return 'hui'
                    // return ls.saveToStorage(field, state[field])
                })
            }
        }
    }
};
