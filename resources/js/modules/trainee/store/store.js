import Vuex from 'vuex';
import idb from './idb'
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        idb
    },
    strict: debug,
    state: {
        count: 15,
        userData: {},
        freshReportsList: [],
        situations: []
    },
    mutations: {
        updateCount(state, payload) {
            Vue.set(state, 'count', state.count + payload);
        },
        makeThms(state, payload) {
            state.count += payload;
        },
        setUserData(state, payload) {
            state.userData = payload;
        },
        setRole: (state, payload) => {
            state.role = payload;
        },
        setPartner: (state, payload) => {
            state.partner = payload;
        },
        setReportsList (state, payload) {
            state.reportsList = payload;
        },
        setFreshReportsList (state, payload) {
            state.freshReportsList = payload;
        },
        setSituations (state, payload) {
            state.situations = payload;
        },
        setCompetencies (state, payload) {
            state.competencies = payload;
        },
        setShift (state, payload) {
            // state.shift = payload;
            state.shift = {
                started_at: payload.mentor_start, //field mentor_start
                finished_at: payload.mentor_finish, //field mentor_finish
                request_start: payload.trainee_start, //field trainee_start
                request_finish: payload.trainee_finish, //field trainee_finish
                shift_num: payload.shift,
                stage_num: payload.stage
            };
        },
        updateReport (state, payload) {
            state.freshReportsList.find((el, index) => {
                if (el.id === payload.id){
                    state.freshReportsList[index].situation = payload.situation;
                    state.freshReportsList[index].fabula = payload.fabula;
                    state.freshReportsList[index].algorithm = payload.algorithm;
                    state.freshReportsList[index].competencies = payload.competencies;
                    state.freshReportsList[index].notes = payload.notes;
                    return el;
                }
            });
        },
        updateFreshReportsList (state, payload) {
            console.log(payload);
            state.freshReportsList.unshift(payload);
        }
    },
    getters: {
        //here write everything you want to mutate in all the components
        getCount: state => {
            return state.count * 2;
        },
        getUserData: state => {
            return state.userData;
        },
        getRole: state => {
            return state.role;
        },
        getPartner: state => {
            return state.partner;
        },
        getReportsList: state => {
            return state.reportsList;
        },
        getFreshReportsList: state => {
            return state.freshReportsList;
        },
        getShift: state => {
            return state.shift;
        },
        getCompetenciesList: state => {
            return state.competencies;
        },
        getSituationsList: state => {
            return state.situations;
        },

        //reports getters
        getReportToUpdate: (state) => (id) => {
            return state.freshReportsList.find(report => report.id === id)
        }

    },
    actions: {

        setUserData (context, payload) {
            context.commit('setUserData', payload);
        },
        setRole (context, payload) {
            context.commit('setRole', payload);
        },
        setPartner (context, payload) {
            context.commit('setPartner', payload);
        },
        setReportsList (context, payload) {
            context.commit('setReportsList', payload);
        },
        setFreshReportsList (context, payload) {

            payload = payload.map(function (el, key) {
                const outputArr = {};
                for (let e in el) {
                    if (Array.isArray(el[e])) {
                        outputArr[e] = el[e].join('\n');
                    }
                    else {
                        outputArr[e] = el[e];
                    }
                }
                return outputArr;

            });
            context.commit('setFreshReportsList', payload);
        },
        setShift (context, payload) {
            context.commit('setShift', payload);
        },
        setSituationsList (context, payload) {
            context.commit('setSituations', payload);
        },
        setCompetenciesList (context, payload) {
            context.commit('setCompetencies', payload);
        },
        updateReport (context, payload) {
            context.commit('updateReport', payload);
        },
        updateFreshReportsList (context, payload) {
            context.commit('updateFreshReportsList', payload);
        },
        //test
        makeSmth (context, payload) {
            context.commit('makeThms', payload);
        }
    },
});
