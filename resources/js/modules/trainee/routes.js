import Trainee from './view/Home';
import HowTo from './view/HowTo';
import Reports from './view/Reports';
import Mentor from './view/Mentor';
import PageNotFound from '@/js/views/PageNotFound';
import VueRouter from "vue-router";

const routes = [

    {
        path: "*",
        component: PageNotFound
    },
    {
        path: '/trainee',
        name: 'trainee',
        component: Trainee,
        title: 'Робочий стіл',
        icon: 'dashboard',
        // redirect: { name: 'people-welcome' },
        children: [
            // {
            //     path: 'how-to',
            //     name: 'how-to',
            //     component: HowTo,
            // },
            // {
            //     path: 'create',
            //     name: 'people-create',
            //     component: Create,
            // },
            // {
            //     path: ':id',
            //     name: 'people-details',
            //     component: Details,
            // },
            // {
            //     path: ':id/edit',
            //     name: 'people-edit',
            //     component: Edit,
            // },
        ],

    },
    {
        path: '/trainee/mentor-info',
        name: 'mentor',
        component: Mentor,
        title: 'Ментор-інфо',
        icon: 'mdi-account-card-details-outline',
    },
    {
        path: '/trainee/reports',
        name: 'reports',
        component: Reports,
        title: 'Звіти',
        icon: 'mdi-file-document-outline',
    },
    {
        path: '/trainee/how-to',
        name: 'how-to',
        component: HowTo,
        title: 'Як користуватись',
        icon: 'mdi-eye-outline',
    },
];
const router = new VueRouter ({
    mode: 'history',
    routes: routes
});

export default router;
