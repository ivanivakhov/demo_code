import Vuex from 'vuex';
// import idb from './idb'
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        // idb
    },
    strict: debug,
    state: {
        count: 15,
        userData: {},
        freshReportsList: []
    },
    mutations: {
        updateCount(state, payload) {
            Vue.set(state, 'count', state.count + payload);
        },
        makeThms(state, payload) {
            state.count += payload;
        },
        setUserData(state, payload) {
            state.userData = payload;
        },
        setRole: (state, payload) => {
            state.role = payload;
        },
        setPartner: (state, payload) => {
            state.partner = payload;
        },
        setReportsList (state, payload) {
            //situation reports
            state.reportsList = payload;
        },
        setFreshReportsList (state, payload) {
            //situation reports
            state.freshReportsList = payload;
        },
        setPartnerReports (state, payload) {
            state.partnerReports = payload;
        },
        setSituations (state, payload) {
            state.situations = payload;
        },
        setCompetencies (state, payload) {
            state.competencies = payload;
        },
        setShift (state, payload) {
            // state.shift = payload;
            state.shift = {
                started_at: payload.mentor_start, //field mentor_start
                finished_at: payload.mentor_finish, //field mentor_finish
                request_start: payload.trainee_start, //field trainee_start
                request_finish: payload.trainee_finish, //field trainee_finish
                shift_num: payload.shift,
                stage_num: payload.stage
            };
        },
        updateReport (state, payload) {
            state.freshReportsList.find((el, index) => {
                if (el.id === payload.id){
                    state.freshReportsList[index].recommendations = payload.recommendations;
                    return el;
                }
            });
        },
        // updateFreshReportsList (state, payload) {
        //     state.freshReportsList.unshift(payload);
        // },
        setSituationsCases (state, payload) {
            state.situationsCases = payload;
        },
        setStageReports (state, payload) {
            state.stageReports = payload;
        }
    },
    getters: {
        //here write everything you want to mutate in all the components
        getCount: state => {
            return state.count * 2;
        },
        getUserData: state => {
            return state.userData;
        },
        getRole: state => {
            return state.role;
        },
        getPartner: state => {
            return state.partner;
        },
        getReportsList: state => {
            //situation reports
            return state.reportsList;
        },
        getFreshReportsList: state => {
            //situation reports
            return state.freshReportsList;
        },
        getStageReports (state) {
            return state.stageReports;
        },
        getShift: state => {
            return state.shift;
        },
        getCompetenciesList: state => {
            return state.competencies;
        },
        getSituationsList: state => {
            return state.situations;
        },
        getPartnerReports: state => {
            return state.partnerReports;
        },
        getSituationsCases: state => {
            return state.situationsCases;
        },

        //reports getters
        getReportToUpdate: (state) => (id) => {
            return state.freshReportsList.find(report => report.id === id)
        }

    },
    actions: {

        setUserData (context, payload) {
            context.commit('setUserData', payload);
        },
        setRole (context, payload) {
            context.commit('setRole', payload);
        },
        setPartner (context, payload) {
            context.commit('setPartner', payload);
        },
        setReportsList (context, payload) {
            //situation reports
            context.commit('setReportsList', payload);
        },
        setFreshReportsList (context, payload) {
            //situation reports
            context.commit('setFreshReportsList', payload);
        },
        setStageReports (context, payload) {
            context.commit('setStageReports', payload);
        },
        setShift (context, payload) {
            context.commit('setShift', payload);
        },
        setSituationsList (context, payload) {
            context.commit('setSituations', payload);
        },
        setCompetenciesList (context, payload) {
            context.commit('setCompetencies', payload);
        },
        setPartnerReports (context, payload) {
            context.commit('setPartnerReports', payload);
        },
        updateReport (context, payload) {
            context.commit('updateReport', payload);
        },
        // updateFreshReportsList (context, payload) {
        //     context.commit('updateFreshReportsList', payload);
        // },
        setSituationsCases (context, payload) {
            context.commit('setSituationsCases', payload);
        },
        //test
        makeSmth (context, payload) {
            context.commit('makeThms', payload);
        }
    },
});
