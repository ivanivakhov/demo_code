import Mentor from './view/Home';
import Reports from "./view/Reports";
import HowTo from "./view/HowTo";
import VueRouter from "vue-router";

const routes = [
    {
        path: '/mentor',
        name: 'mentor',
        component: Mentor,
        title: 'Робочий стіл',
        icon: 'dashboard',
        // redirect: { name: 'people-welcome' },
        children: [
            // {
            //     path: 'welcome',
            //     name: 'people-welcome',
            //     component: Welcome,
            // },
            // {
            //     path: 'create',
            //     name: 'people-create',
            //     component: Create,
            // },
            // {
            //     path: ':id',
            //     name: 'people-details',
            //     component: Details,
            // },
            // {
            //     path: ':id/edit',
            //     name: 'people-edit',
            //     component: Edit,
            // },
        ],
    },
    {
        path: '/mentor/trainee-info',
        name: 'trainee',
        component: Mentor,
        title: 'Стажер-інфо',
        icon: 'mdi-account-card-details-outline',
    },
    {
        path: '/mentor/reports',
        name: 'reports',
        component: Reports,
        title: 'Звіти',
        icon: 'mdi-file-document-outline',
    },
    {
        path: '/trainee/how-to',
        name: 'how-to',
        component: HowTo,
        title: 'Як користуватись',
        icon: 'mdi-eye-outline',
    },
];
const router = new VueRouter ({
    mode: 'history',
    routes: routes
});

export default router;
