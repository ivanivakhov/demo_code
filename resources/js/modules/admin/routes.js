import Admin from './view/Home';
import BattalionTroop from "./view/BattalionTroop";
import Teams from "./view/Teams";
import Roles from "./view/Roles";
import VueRouter from "vue-router";

const routes = [
    {
        path: '/admin',
        name: 'admin',
        component: Admin,
        title: 'Головна',
        icon: 'fa-users',
        // redirect: { name: 'people-welcome' },
        children: [
            // {
            //     path: 'welcome',
            //     name: 'people-welcome',
            //     component: Welcome,
            // },
            // {
            //     path: 'create',
            //     name: 'people-create',
            //     component: Create,
            // },
            // {
            //     path: ':id',
            //     name: 'people-details',
            //     component: Details,
            // },
            // {
            //     path: ':id/edit',
            //     name: 'people-edit',
            //     component: Edit,
            // },
        ],
    },
    {
        path: '/admin/teams',
        name: 'teams',
        component: Teams,
        title: 'Команди',
        icon: ''
    },
    {
        path: '/admin/roles',
        name: 'roles',
        component: Roles,
        title: 'Ролі',
        icon: ''
    },
    {
        path: '/admin/department',
        name: 'department',
        component: BattalionTroop,
        title: 'Батальон/рота',
        icon: ''
    }
];
const router = new VueRouter ({
    mode: 'history',
    routes: routes
});

export default router;
