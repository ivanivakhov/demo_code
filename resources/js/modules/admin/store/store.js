import Vuex from 'vuex';
// import createPersistedState from 'vuex-persistedstate';
// import messages from '@/js/modules/messages/store';
// import people from '@/js/modules/people/store';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        // messages,
        // people,
    },
    // plugins: [createPersistedState({
    //     key: 'kazoo',
    // })],
    strict: debug,
    state: {
        count: 15,
        userData: {},
        freshReportsList: []
    },
    mutations: {
        updateCount(state, payload) {
            Vue.set(state, 'count', state.count + payload);
        },
        makeThms(state, payload) {
            state.count += payload;
        },
        setUserData(state, payload) {
            state.userData = payload;
        },
        setRole: (state, payload) => {
            state.role = payload;
        },
        setPartner: (state, payload) => {
            state.partner = payload;
        },
        setReportsList (state, payload) {
            state.reportsList = payload;
        },
        setFreshReportsList (state, payload) {
            state.freshReportsList = payload;
        },
        updateFreshReportsList (state, payload) {
            let asdf = state.freshReportsList.find((el, index) => {
                if (el.id === payload.id){
                    state.freshReportsList[index].situation = payload.situation;
                    state.freshReportsList[index].fabula = payload.fabula;
                    state.freshReportsList[index].algorithm = payload.algorithm;
                    state.freshReportsList[index].competencies = payload.competencies;
                    state.freshReportsList[index].notes = payload.notes;
                    console.log(state.freshReportsList[index]);
                    return el;
                }
            });
            console.log(asdf);
            // context.commit('setFreshReportsList', payload);
        },
    },
    getters: {
        //here write everything you want to mutate in all the components
        getCount: state => {
            return state.count * 2;
        },
        getUserData: state => {
            return state.userData;
        },
        getRole: state => {
            return state.role;
        },
        getPartner: state => {
            return state.partner;
        },
        getReportsList: state => {
            return state.reportsList;
        },
        getFreshReportsList: state => {
            return state.freshReportsList;
        },
    },
    actions: {

        setUserData (context, payload) {
            context.commit('setUserData', payload);
        },
        setRole (context, payload) {
            context.commit('setRole', payload);
        },
        setPartner (context, payload) {
            context.commit('setPartner', payload);
        },
        setReportsList (context, payload) {
            context.commit('setReportsList', payload);
        },
        setFreshReportsList (context, payload) {
            context.commit('setFreshReportsList', payload);
        },
        updateFreshReportsList (context, payload) {
            context.commit('updateFreshReportsList', payload);
        },
        //test
        makeSmth (context, payload) {
            context.commit('makeThms', payload);
        }
    },
});
