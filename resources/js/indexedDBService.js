// if installed from npm use 'openDB'
import { openDB } from 'idb'
const dbPromise = _ => {
    if (!('indexedDB' in window)) {
        throw new Error('Browser does not support IndexedDB')
    }
    // if installed from npm use 'openDB'
    return openDB('VueTodoDB', 1, {
        upgrade(db, oldVersion, newVersion, transaction) {
            console.log('upgrade');
            console.log(db);
            console.log(oldVersion);
            console.log(newVersion);
            console.log(transaction);

            if (!db.objectStoreNames.contains('todos')) {
                db.createObjectStore('todos')
            }
            if (!db.objectStoreNames.contains('completed')) {
                db.createObjectStore('completed')
            }
        },
        blocked() {
            console.log('blocked');
        },
        blocking() {
            console.log('blocking');
        },
        terminated() {
            console.log('terminated');
        },
    })
    return openDB('VueTodoDB', 1, upgradeDb => {
        if (!upgradeDb.objectStoreNames.contains('todos')) {
            upgradeDb.createObjectStore('todos')
        }
        if (!upgradeDb.objectStoreNames.contains('completed')) {
            upgradeDb.createObjectStore('completed')
        }
    })
}

const saveToStorage = async (storeName, tasks) => {

    try {
        const db = await dbPromise();
        const tx = db.transaction(storeName, 'readwrite');
        const store = tx.objectStore(storeName);
        store.put(tasks, storeName);
        return tx.complete;
    } catch (error) {
        console.log(error);

        return error;
    }
}

const checkStorage = async storeName => {
    try {
        const db = await dbPromise();
        const tx = db.transaction(storeName, 'readonly');
        const store = tx.objectStore(storeName);
        // console.log(store.get('completed'));
        return store.get(storeName);
    } catch (error) {
        return error;
    }
}

export default {
    checkStorage,
    saveToStorage
}
