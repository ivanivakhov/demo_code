/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import Routes from '@/js/modules/mentor/routes';
import Vuetify from '@/plugins/vuetify';
import App from '@/js/modules/mentor/App';
import store from '@/js/modules/mentor/store/store';
import Axios from 'axios'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
Vue.prototype.$http = Axios;


const initialState = JSON.parse(window.__INITIAL_STATE__);

const app = new Vue({
    el: '#app',
    store: store,
    vuetify: Vuetify,
    router: Routes,
    render: h => h(App),
});

export default {app, initialState};
