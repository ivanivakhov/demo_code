/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import Routes from '@/js/modules/admin/routes';
import Vuetify from '@/plugins/vuetify';
import App from '@/js/views/App';
import store from '@/js/modules/admin/store/store';
import Axios from 'axios'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
Vue.prototype.$http = Axios;
// const checkCondition = true;
// console.log(localStorage.getItem('user'));
// Routes.beforeEach((to, from, next) => {
//     if (to.matched.some(record => record.meta.conditionalRoute)) {
//         // this route requires condition to be accessed
//         // if not, redirect to home page.
//         if (!checkCondition) {
//             //check codition is false
//             console.log(123);
//             next({ path: '/'})
//         } else {
//             //check codition is true
//             next()
//         }
//     } else {
//         next() // make sure to always call next()!
//     }
// });

// Vue.use(timeline, { store, Routes });

const initialState = JSON.parse(window.__INITIAL_STATE__);

// console.log(Routes);
// console.log(123);

const app = new Vue({
    el: '#app',
    store: store,
    vuetify: Vuetify,
    router: Routes,
    render: h => h(App),
});

export default {app, initialState};
