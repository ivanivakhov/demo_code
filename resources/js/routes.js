import VueRouter from 'vue-router';

const initialState = JSON.parse(window.__INITIAL_STATE__);

import admin from '@/js/modules/admin/routes';
import mentor from '@/js/modules/mentor/routes';
import trainee from '@/js/modules/trainee/routes';
import PageNotFound from '@/js/views/PageNotFound';

const importedRoutes = {
    admin: admin,
    mentor: mentor,
    trainee: trainee
};



const currentRole = initialState.role;

let selectedRoute;
for (let key in importedRoutes) {
    if (key === currentRole) {
         selectedRoute = importedRoutes[key]
    }
}

Vue.use(VueRouter);
const baseRoutes = [
    {
        path: "*",
        component: PageNotFound
    }
];

//todo:: change this optionally. If this is mentor show only mentors routes.

const routes = baseRoutes.concat(selectedRoute);

const router = new VueRouter ({
    mode: 'history',
    routes: routes
});

export default router;
